'use strict';
angular.module('vcm-tsp', ['ui.bootstrap', 'ui.router', 'ngResource', 'angularMoment', 'sigmajs-ng', 'toastr', 'plotly'])

    .run(function ($rootScope, VERSION) {
        $rootScope.VERSION = VERSION;
        $rootScope.toast = {};
    	$rootScope.user = {};
    	$rootScope.$on('Alert', function (event, data) {
    		console.log(data); // 'Data to send'
    		data.message
		});

    })

	.config(function ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig) {

	    $httpProvider.interceptors.push('TimeStampInterceptor');
	    $httpProvider.interceptors.push('httpInterceptor');
	    
        $urlRouterProvider.otherwise('/');
        $stateProvider.state('site', { 
            'abstract': true,
            views: {
                'navbar@': {
                    templateUrl: 'scripts/components/navbar/navbar.html',
                    controller: 'NavbarController'
                },
                'footer@':{
                	templateUrl: 'scripts/components/footer/footer.html',
                	controller: 'FooterController'
                },
                'loader@':{
                	templateUrl: 'scripts/components/loader/loader.html'
                }
            }
        });
        
        angular.extend(toastrConfig, {
            maxOpened: 3,    
            positionClass: 'toast-bottom-right'
          });

	})
	
;