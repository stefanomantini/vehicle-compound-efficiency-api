'use strict';

angular.module('vcm-tsp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('results', {
                parent: 'site',
                url: '/results',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/results/results.html',
                        controller: 'ResultsController'
                    }
                }
            })
    });
