'use strict';

angular.module('vcm-tsp')
    .controller('ShortestPathDiagramController', function ($scope, $stateParams, $uibModalInstance) {
    	$scope.sigmaGraph = $stateParams.sigmaGraph;
    	    	
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

    })
;