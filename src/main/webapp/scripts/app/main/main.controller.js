'use strict';

angular.module('vcm-tsp')
.controller('MainController', function ($scope, $http, $location, ShortestPathFactory, toastr, $timeout) {
	//Generic API Controller requests
	var uri = $location.protocol() + "://" + $location.host() + ":" +$location.port();
	var swagger = uri + "/v2/api-docs"
	$scope.apiResponse = {};
	
	$scope.accordionStart = false;
	
	/**
	 * Dropdowns
	 */
	
		$scope.dropdowns = {};
		$scope.dropdowns.solver = {
				"brute-force":"Brute Force",
				"nearest-neighbour":"Nearest Neighbour",
				"repeated-nearest-neighbour":"Repeated Nearest Neighbour",
				"nissan-morrocco-tangiers":"Nissan Morrocco Tangiers",
				"sampled-repeated-nearest-neighbour":"Sampled Repeated Nearest Neighbour",
				"divide-and-conquer":"Divide & Conquer",
				"unsorted":"Unsorted"
		};				

		$scope.dropdowns.opt = {
				"":"None",
				"pairwise-exchange":"Pairwise Exchange",
				"segment-reverse":'Segment Reverse (Buggy)'
		}
		$scope.dropdowns.dist ={
				"ed1122":"Euclidian Distance",
				"ed12":"Pickup only Euclidian Distance",
				"wed1122":"Weighted Euclidian Distance"
		};
		$scope.dropdowns.data = {
				"sample":"Server Sample",
				"random":"Random",
				"random-seed":"Seeded Random",
				"data":"'POST' Data",
				"mona":"Mona Lisa TSP Challenge (100,000)"
		}
		
		/**
		 * Presets
		 */
		$scope.dropdowns.presets = {};
		$scope.dropdowns.presets.bruteForce = function(){
			$scope.request = {
					"solver": "brute-force",
					"calc": "wed1122",
					"sampleType": "random-seed",
					"nVehicles": "6",
					"walkWeight": "5",
					"seed":"149"
			};
		};
		$scope.dropdowns.presets.nearestNeighbour = function(){
			$scope.request = {
					"solver": "nearest-neighbour",
					"calc": "wed1122",
					"sampleType": "random-seed",
					"nVehicles": "6",
					"walkWeight": "5",
					"seed":"149"
			};
		}
		
		$scope.dropdowns.presets.repeatedNearestNeighbour = function(){
				$scope.request = {
						"solver": "repeated-nearest-neighbour",
						"calc": "wed1122",
						"sampleType": "random-seed",
						"nVehicles": "6",
						"walkWeight": "5",
						"seed":"149"
				};    	
		}
		$scope.dropdowns.presets.sampledRepeatedNearestNeighbour = function(){
				$scope.request = {
						"solver": "sampled-repeated-nearest-neighbour",
						"calc": "wed1122",
						"sampleType": "random-seed",
						"nVehicles": "6",
						"walkWeight": "5",
						"seed":"149"
				};    	
		}
		$scope.dropdowns.presets.repeatedNearestNeighbour2Opt = function(){
				$scope.request = {
						"solver": "repeated-nearest-neighbour",
						"calc": "wed1122",
						"sampleType": "random-seed",
						"alterTour": "pairwise-exchange",
						"nVehicles": "6",
						"walkWeight": "5",
						"seed":"149"
				};    	
		}
		$scope.dropdowns.presets.sampledNearestNeighbour2Opt = function(){
			$scope.request = {
					"solver": "sampled-repeated-nearest-neighbour",
					"calc": "wed1122",
					"sampleType": "random-seed",
					"alterTour": "pairwise-exchange",
					"nVehicles": "6",
					"walkWeight": "5",
					"seed":"149"
			};    	
	}
		$scope.dropdowns.presets.nissanMethod = function(){
				$scope.request = {
						"solver": "nissan-morrocco-tangiers",
						"calc": "wed1122",
						"sampleType": "random-seed",
						"nVehicles": "6",
						"walkWeight": "5",
						"seed":"149"
				};    	
		}
		$scope.dropdowns.presets.divideConquer = function(){
			$scope.request = {
					"solver": "divide-and-conquer",
					"calc": "wed1122",
					"sampleType": "random-seed",
					"nVehicles": "6",
					"walkWeight": "5",
					"seed":"149"
			};    	
		}
		$scope.dropdowns.presets.unsorted = function(){
			$scope.request = {
					"solver": "unsorted",
					"calc": "wed1122",
					"sampleType": "random-seed",
					"nVehicles": "6",
					"walkWeight": "5",
					"seed":"149"
			};    	
		}

	$scope.responseObject = {};
	$scope.response = {};

	$scope.data = {};
	$scope.data.nodes = [];
	$scope.data.edges = [];
	$scope.sigmaGraph = {};
	$scope.sigmaGraph.nodes = [];
	$scope.sigmaGraph.edges = [];




	// Simple GET request example:
	$http({
		method: 'GET',
		url: swagger
	}).then(function successCallback(response) {
		$scope.apiResponse = response.data;
		$scope.responseObject = response.data.definitions.SolverRequestObject.properties;
		var result = {};
		angular.forEach(response.data.definitions.SolverRequestObject.properties, function(value, key) {
			result[key] = value.type
		});
		result["data"] = "[]";
		$scope.SolverRequestObject = result;

	}, function errorCallback(response) {
		alert("Check swagger is working");
	});


	$scope.solveUsing = function(solver){
		$scope.request.solver = solver;
	}

	$scope.calcUsing = function(calc){
		$scope.request.calc=calc;
	}

	$scope.sampleUsing = function(sample){
		$scope.request.sampleType = sample;
	}

	$scope.alterUsing = function(alter){
		$scope.request.alterTour = alter;
	}



	$scope.remove = function(vehicle){    	
		var index = $scope.response.vehicles.indexOf(vehicle);
		$scope.response.vehicles.splice(index, 1);     
	};



	$scope.request = {
	};

	$scope.vehicles = {};


	$scope.clear = function(){
		$scope.request = {};
		$scope.sigmaGraph = {};
		$scope.data = {};
		$scope.response = {};
		$scope.responseRequest = {};
	}

	
	$scope.solve = function(){  
		/**warnings*/
		if($scope.request.solver == "brute-force" && 
				($scope.request.nVehicles >= 9)){
			toastr.warning("Brute force cannot handle 9+ vehicles, Your request has been modified");
			$scope.request.nVehicles = 8; 
		}
		if($scope.request.sampleType == "mona"){
			toastr.warning("The mona Lisa Dataset is made up of 100,000 Nodes this may take a while: the best known results for a tour is 5,757,191");
		}
		
		
		/**reset graph data*/
		var currentNode = {};
		var nextNode = {};
		$scope.sigmaGraph = {};
		$scope.sigmaGraph.nodes = [];
		$scope.sigmaGraph.edges = [];

		//http request for data with params
		$http.post(uri + '/api/solver/shortestPath', $scope.request)
		//subsequently build the sigmaGraph object from data
		.then(function successCallback(response, config) {
			var time = response.config.responseTimestamp - response.config.requestTimestamp;
			toastr.success("Request took " + (time/1000) +" seconds");
			$scope.responseRequest = response.data.request;
			$scope.response = response.data.shortestTour;
			
			angular.forEach($scope.response.vehicles, function(value, key) {
				currentNode = {
						"id": value.invariant + "-C",
						"label": value.invariant + "-C",
						"x": value.currentParkingPosition.x,
						"y": value.currentParkingPosition.y,
						"size": 8
				};
				nextNode = {
						"id": value.invariant + "-N",
						"label": value.invariant + "-N",
						"x": value.nextParkingPosition.x,
						"y": value.nextParkingPosition.y,
						"size": 4,
						"color": "#009688"
				};

				if(currentNode.id == "STARTNODE"){
					$scope.sigmaGraph.nodes.push(currentNode);
//					$scope.sigmaGraph.edges.push(currentEdge);
				}else{
					$scope.sigmaGraph.nodes.push(currentNode);
					$scope.sigmaGraph.nodes.push(nextNode);
				}
			});
			$scope.sigmaGraph.nodes.splice(1, 1);
			angular.forEach($scope.sigmaGraph.nodes, function(value, key) {
				if(key+1 < $scope.sigmaGraph.nodes.length){
					var currentEdge = {
							"id": value.id + '-CE',
							"source": value.id,
							"target": $scope.sigmaGraph.nodes[key+1].id
					};
					$scope.sigmaGraph.edges.push(currentEdge);
				}
			});



		}).then(function errorCallback(response, config) {
			//toastr.error("HTTP Request Failed");
		})

		;
	};



		


})

;
