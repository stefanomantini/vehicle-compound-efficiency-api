'use strict';

angular.module('vcm-tsp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('metrics', {
                parent: 'site',
                url: '/metrics',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'metrics.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/metrics/metrics.html',
                        controller: 'MetricsController'
                    }
                }
            });
    });
