'use strict';

angular.module('vcm-tsp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('vehicle-generation', {
                parent: 'site',
                url: '/vehicle-generation',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/vehicle-generation/generator.html',
                        controller: 'VehicleGeneratorController'
                    }
                }
            });
    });
