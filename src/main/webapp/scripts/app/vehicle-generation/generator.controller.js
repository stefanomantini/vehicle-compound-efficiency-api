'use strict';

angular.module('vcm-tsp')
    .controller('VehicleGeneratorController', function ($scope, DataRandomSeed, DataRandom, DataStatic, DataMona, toastr) {
    	$scope.response = {};
    	$scope.nVehicles = {};
    	$scope.nVehicles.n = 5;
    	
        $scope.DataRandomSeed = function (nVehicles) {
        	DataRandomSeed.query({nVehicles:nVehicles}, function(result) {
        		$scope.response.data = angular.toJson(result, true);
            });
        };
        
        $scope.DataRandom = function (nVehicles) {
        	DataRandom.query({nVehicles:nVehicles}, function(result) {
        		$scope.response.data = angular.toJson(result, true);
            });
        };
        
        $scope.DataStatic = function (nVehicles) {
        	DataStatic.query({nVehicles:nVehicles}, function(result) {
        		$scope.response.data = angular.toJson(result, true);
            });
        };
        
        $scope.DataMona = function (nVehicles) {
			toastr.warning("The mona Lisa Dataset is made up of 100,000 Nodes this may take a while: the best known results for a tour is 5,757,191");
        	DataMona.query({nVehicles:nVehicles}, function(result) {
        		$scope.response.data = angular.toJson(result, true);
            });
        };
        
        
    })
;
