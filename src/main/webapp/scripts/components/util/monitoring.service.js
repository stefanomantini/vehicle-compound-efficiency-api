'use strict';

angular.module('vcm-tsp')
    .factory('MonitoringService', function ($rootScope, $http) {
        return {
            getMetrics: function () {
                return $http.get('metrics/metrics').then(function (response) {
                    return response.data;
                });
            },

            checkHealth: function () {
                return $http.get('metrics/health').then(function (response) {
                    return response.data;
                });
            },

            threadDump: function () {
                return $http.get('metrics/dump').then(function (response) {
                    return response.data;
                });
            }
        };
    });
