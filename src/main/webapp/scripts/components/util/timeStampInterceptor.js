'use strict';
angular.module('vcm-tsp')
	.factory('TimeStampInterceptor', [function(AlertService) {
	    var timestampMarker = {
	        request: function(config) {
	            config.requestTimestamp = new Date().getTime();
	            return config;
	        },
	        response: function(response) {
	            response.config.responseTimestamp = new Date().getTime();
	            return response;
	        }
	    };
	    return timestampMarker;
	}])
;

