(function() {
	'use strict';
	angular.module('sigmajs-ng', [])
	
	.directive('sigmajs', function() {
		//random id, so that multiple instances can be put on a single page
		var divId = 'sigmjs-dir-container-'+Math.floor((Math.random() * 999999999999))+'-'+Math.floor((Math.random() * 999999999999))+'-'+Math.floor((Math.random() * 999999999999));
		return {
			restrict: 'E',
			template: '<div id="'+divId+'" style="width: 100%;height: 100%;"></div>',
			scope: {
				//@ reads attribute value for two-way binding and functions
				graph: '=',
				width: '@',
				height: '@',
				releativeSizeNode: '='
			},
			link: function (scope, element, attrs) {
				// initialize sigma:
				var s = new sigma({
					container: divId,
					settings: {
						defaultNodeColor: '#ec5148',
						labelThreshold: 4,
						scalingMode: "outside"
					}
				});
				//inefficient - runs on every digest
				scope.$watch('graph', function(newVal,oldVal) {
					s.graph.clear();
					s.graph.read(scope.graph);
					s.refresh();
//					if(scope.releativeSizeNode) {
//						//this feature needs the plugin to be added
//						sigma.plugins.relativeSize(s, 2);
//					}
				});
	
				scope.$watch('width', function(newVal,oldVal) {
					element.children().css("width",scope.width);
					s.refresh();
					//hack so that it will be shown instantly
					window.dispatchEvent(new Event('resize')); 
				});
				scope.$watch('height', function(newVal,oldVal) {
					element.children().css("height",scope.height);
					s.refresh();
					//hack so that it will be shown instantly
					window.dispatchEvent(new Event('resize'));
				});
	
				element.on('$destroy', function() {
					s.graph.clear();
				});
			}
		};
	});
})();
