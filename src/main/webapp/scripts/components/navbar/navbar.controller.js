'use strict';

angular.module('vcm-tsp')

    .controller('NavbarController', function ($scope, $state, $rootScope, $http, CodeShipAPI, $uibModal) {
        $scope.logout = function () {
            $state.go('home');
        };

        $scope.state = false;
        $scope.toggleState = function() {
        	$scope.state = !$scope.state;
        };
        
        $scope.items = ['item1', 'item2', 'item3'];
        $scope.info = function () {

            var modalInstance = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'myModalContent.html',
              controller: 'InfoModalInstanceCtrl',
              size: "lg",
              resolve: {
                items: function () {
                  return $scope.items;
                }
              }
            });
        };

        $http({
            method: 'GET',
            url: CodeShipAPI.uri()
          }).then(function successCallback(response) {

            var dates = [];
            var today = new Date();

            //push dates into an array for ease of sorting
            angular.forEach(response.data.builds, function(value, key) {
              if(value.status === "success"){
                dates.push(value.finished_at);
              }
            });

            //sort array closest to today first
            dates.sort(function(a, b) {
                var distancea = Math.abs(today - a);
                var distanceb = Math.abs(today - b);
                return distancea - distanceb; // sort a before b when the distance is smaller
            });
            $scope.build = dates[0];

            }, function errorCallback(response) {
            });

    })



    .directive('sidebarDirective', function() {
        return {
            link : function(scope, element, attr) {
                scope.$watch(attr.sidebarDirective, function(newVal) {
                      if(newVal)
                      {
                        element.addClass('show');
                        return;
                      }
                      element.removeClass('show');
                });
            }
        };
    })

    
    .controller('InfoModalInstanceCtrl', function ($scope, $uibModalInstance, items) {	
		  $scope.cancel = function () {
		    $uibModalInstance.dismiss('cancel');
		  };
    })

;
