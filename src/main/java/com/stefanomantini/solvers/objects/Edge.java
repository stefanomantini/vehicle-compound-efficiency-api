package com.stefanomantini.solvers.objects;

/**
 * Represents an edge, holds two vehicles
 * as well as an associated weight
 * @author Stefano Mantini
 *
 */
public class Edge {

	private Vehicle startNode;
	private Vehicle endNode;
	private double euclidianDistance;
	
	public Edge(Vehicle startNode, Vehicle endNode, double euclidianDistance){
		this.startNode = startNode;
		this.endNode = endNode;
		this.euclidianDistance = euclidianDistance;
	}

	/**
	 * @return the startNode
	 */
	public Vehicle getStartNode() {
		return startNode;
	}
	/**
	 * @param startNode the startNode to set
	 */
	public void setStartNode(Vehicle startNode) {
		this.startNode = startNode;
	}
	/**
	 * @return the endNode
	 */
	public Vehicle getEndNode() {
		return endNode;
	}
	/**
	 * @param endNode the endNode to set
	 */
	public void setEndNode(Vehicle endNode) {
		this.endNode = endNode;
	}
	/**
	 * @return the euclidianDistance
	 */
	public double getEuclidianDistance() {
		return euclidianDistance;
	}
	/**
	 * @param euclidianDistance the euclidianDistance to set
	 */
	public void setEuclidianDistance(double euclidianDistance) {
		this.euclidianDistance = euclidianDistance;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Edge [startNode=" + startNode + ", endNode=" + endNode + ", euclidianDistance=" + euclidianDistance
				+ "]";
	}

}
