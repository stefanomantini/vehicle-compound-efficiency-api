package com.stefanomantini.solvers.alterations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.stefanomantini.solvers.objects.Tour;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.solvers.util.Coord;
import com.stefanomantini.solvers.util.EuclidianDistance;
import com.stefanomantini.web.util.SolverRequestObject;

/**
 * Class not currently in use
 * @author Stefano Mantini
 */
public class SegmentReverse {
	
	private static final Logger logger = LoggerFactory.getLogger(SegmentReverse.class);
	/**
	 * Try to alter tour by reversing Segments
	 * @param shortestTour
	 * @return
	 */
	public static Tour alterTour(Tour shortestTour, SolverRequestObject request){
		Vehicle start = shortestTour.getVehicles().get(0);
		shortestTour.getVehicles().remove(0);
		Tour improvedTour = null;
		
		//get all combinations of segment start-end points
		ArrayList<Coord<?,?>> startEndPairs = allSegments(shortestTour.getVehicles());
		for(Coord<?, ?> pair : startEndPairs){
			int startC = (int) pair.getX();
			int endC = (int) pair.getY();
			improvedTour = reverseSegmentIfBetter(shortestTour, startC, endC, request);
		}
		improvedTour.getVehicles().set(0, start);
		improvedTour.setTotalEuclidianDistance(EuclidianDistance.calcCollectionFromRequest(improvedTour.getVehicles(), request));
		return improvedTour;
	}
	
	
	/**
	 * Generate all segments as start, end pairs
	 * @param tour
	 * @return
	 */
	public static ArrayList<Coord<?, ?>> allSegments(ArrayList<Vehicle> tour){
		ArrayList<Coord<?, ?>> startEndPairs = new ArrayList<Coord<?, ?>>();
		for(int i = 0 ; i < tour.size()-1; i ++){//outer
			  for(int j = i+1 ; j < tour.size()-1; j ++){//inner
			    Coord<?,?> temp = new Coord<Object, Object>(i, j);
			    startEndPairs.add(temp);
			  }
			}
		return startEndPairs;
	}
	
	
	/**
	 * Reverse Segment if the EDistance is better
	 * @param tour
	 * @param start
	 * @param end
	 * @param request
	 * @return
	 */
	public static Tour reverseSegmentIfBetter(Tour original, int start, int end, SolverRequestObject request){	
		Tour vehicles = original;
		
		//reverse segment if better
		List<Vehicle> segment = new ArrayList<Vehicle>(vehicles.getVehicles().subList(start, end+1));
		double edForSegment = EuclidianDistance.calcCollectionFromRequest((ArrayList<Vehicle>) segment, request);
		Collections.reverse(segment);
		double edForReversed = EuclidianDistance.calcCollectionFromRequest((ArrayList<Vehicle>) segment, request);
		
		logger.debug("ED for segment:" + edForSegment + " ED for reversed:" + edForReversed);

		//is segment better?
		if(edForSegment < edForReversed){
			logger.debug("No Improvement made");
			 return original;
		 }else{
			//remove start-end from tour
			List<Vehicle> toBeRemoved = new ArrayList<Vehicle>();
			for(int i=start;i<end;i++){
				toBeRemoved.add(vehicles.getVehicles().get(i));
			}
			logger.debug("Removing from original: N="+toBeRemoved.size()+ " Segment Size="+(end-start));
			System.out.println("PRE"+vehicles.getVehicles());
			vehicles.getVehicles().removeAll(toBeRemoved);
			System.out.println("POST"+vehicles.getVehicles());
			
			//add segment to tour
			vehicles.setTotalEuclidianDistance(EuclidianDistance.calcCollectionFromRequest(vehicles.getVehicles(), request));
			return vehicles ;
		 }
	}

	

	/**
	 * Reverse a tour between two points and return the amended tour list
	 * @param preAlteredTour
	 * @param start
	 * @param end
	 * @return
	 */
	private static ArrayList<Vehicle> reversed(ArrayList<Vehicle> preAlteredTour, int start, int end) {
		
		//create new collection and add the items in the segment
		ArrayList<Vehicle> segmentToReverse = new ArrayList<Vehicle>();
		
		for(int i = start;i>end;i++){
			segmentToReverse.add(preAlteredTour.get(i));
		}
		
		//reverse segment
		Collections.reverse(segmentToReverse);
	    
		//remove segment from original collection
		int it = 0;
		for (Iterator<Vehicle> iterator = preAlteredTour.iterator(); iterator.hasNext();) {
		    Vehicle vehicle = iterator.next();
		    	if(it >= start && it <= end){
		    		iterator.remove();
		    	}
		    	it++;
		}
		
		//add the new reversed segment to original collection
		preAlteredTour.addAll(start, segmentToReverse);
		
		return preAlteredTour;
	}


}
