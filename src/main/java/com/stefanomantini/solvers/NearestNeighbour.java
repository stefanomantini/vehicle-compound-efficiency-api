package com.stefanomantini.solvers;

import java.util.ArrayList;
import java.util.Iterator;

import com.stefanomantini.solvers.objects.Tour;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.solvers.util.EuclidianDistance;
import com.stefanomantini.web.util.SolverRequestObject;

/**
 * Nearest Neighbour:
 * Start at any city after the starting node, then look through the 
 * remaining vehicles and find the nearest city to that node, repeat 
 * until there are no more cities to visit
 * Loosely based on Peter Norvig's "nn_tsp" from: 
 * -norvig.com/ipython/TSP.ipynb
 * @author Stefano Mantini
 *
 */
public class NearestNeighbour {
	
	/**
	 * Main method, dispatches to other methods and handles 
	 * initial startup of a tour (finding startnode and next)
	 * @param unvisited
	 * @param request
	 * @return
	 */
	public static Tour calculateNearestNeighbour(ArrayList<Vehicle> unvisited, SolverRequestObject request) {
		ArrayList<Vehicle> visited = new ArrayList<Vehicle>();
		Tour shortestTour = new Tour();
		
		//find start node
		for (Iterator<Vehicle> iterator = unvisited.iterator(); iterator.hasNext(); ) {
		    Vehicle vehicle = iterator.next();
		    	//iterator.remove();
			if(vehicle.isStartNode()){
				visited.add(vehicle);
				iterator.remove();
				break;
			}
		}
		
			//get shortest, remove from unvisited add to visited
			while(unvisited.size() >= 1){
				Vehicle C = nearestNeighbour(visited.get(visited.size()-1), unvisited, request);
				unvisited.remove(C);
				visited.add(C);
			}
			shortestTour.setVehicles(visited);
			shortestTour.setTotalEuclidianDistance(EuclidianDistance.calcCollectionFromRequest(visited, request));

		return shortestTour;
	}
	
	/**
	 * Gets the nearest neighbour to a given vehicle, 
	 * given the next vehicle and unvisited nodes
	 * @param next
	 * @param unvisited
	 * @return closest vehicle
	 */
	public static Vehicle nearestNeighbour(Vehicle next, ArrayList<Vehicle> unvisited, SolverRequestObject request){
		double max = Double.MAX_VALUE;
		Vehicle shortestNextNode = null;
		for(Vehicle uVehicle: unvisited){

			double possibleDistance = EuclidianDistance.calcPairFromRequest(next, uVehicle, request); //calculates for current and next
			if(possibleDistance < max){
				max = possibleDistance;
				shortestNextNode = uVehicle;
			}
		}
		return shortestNextNode;
	}
}
