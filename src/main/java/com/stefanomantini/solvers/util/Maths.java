package com.stefanomantini.solvers.util;

public class Maths {

	/**
	 * 10% quicker than Math.sqrt.d
	 * @param d
	 * @return
	 */
    public static final double sqrt(final double d) {
        //return Math.sqrt(d); //no diff (jni overhead.)
    	return d*invSqrt(d); // ~10% faster than Math.sqrt.
    }
    
    /**
     * fast inverse square root.
     * originally from quake 3:
     * works by making a clever guess as to the starting point
     * for newton's method. 1 pass is a good approximation.
     * http://en.wikipedia.org/wiki/Fast_inverse_square_root
     * @param x
     * @return
     */
    public static double invSqrt(double x) {
        final double xhalf = 0.5d*x;
        long i = Double.doubleToLongBits(x);
        i = 0x5fe6eb50c7b537a9L - (i >> 1);
        x = Double.longBitsToDouble(i);
        x *= (1.5d - xhalf*x*x); // pass 1
        x *= (1.5d - xhalf*x*x); // pass 2
        x *= (1.5d - xhalf*x*x); // pass 3               
        x *= (1.5d - xhalf*x*x); // pass 4
        return x;
    }

}
