package com.stefanomantini.solvers.util;

import java.util.Comparator;

import com.stefanomantini.solvers.objects.Tour;

public class TourComparator implements Comparator<Tour> {
	/**
	 * Compares two tours based on 
	 * pre-calculated ED
	 * @param o1
	 * @param o1
	 * @return shorter edge
	 */
    @Override
    public int compare(Tour o1, Tour o2) {
        return (int) (o1.getTotalEuclidianDistance() - o2.getTotalEuclidianDistance());
    }

}
