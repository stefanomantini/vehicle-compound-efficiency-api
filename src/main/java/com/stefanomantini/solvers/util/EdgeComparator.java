package com.stefanomantini.solvers.util;

import java.util.Comparator;

import com.stefanomantini.solvers.objects.Edge;

public class EdgeComparator implements Comparator<Edge> {
		/**
		 * Compares two edges based on 
		 * pre-calculated ED
		 * @param o1
		 * @param o2
		 * @return shorter edge
		 */
		@Override
	    public int compare(Edge o1, Edge o2) {
	    	return (int) ((o1.getEuclidianDistance()) - (o2.getEuclidianDistance()));
	    }
	}
