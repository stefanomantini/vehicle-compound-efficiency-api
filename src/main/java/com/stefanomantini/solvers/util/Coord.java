package com.stefanomantini.solvers.util;

/**
 * Coordinate object (Based on KeyValue Pair class from guava)
 * @author Stefano Mantini
 *
 * @param <X>
 * @param <Y>
 */
public class Coord<X, Y> {

    private double x; //first member of pair
    private double y; //second member of pair

	public Coord(double x, double y) {
        this.setX(x);
        this.setY(y);
    }

	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @param x2 the x to set
	 */
	public void setX(double x2) {
		this.x = x2;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y2 the y to set
	 */
	public void setY(double y2) {
		this.y = y2;
	}
	
    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Coord [x=" + x + ", y=" + y + "]";
	}
}
