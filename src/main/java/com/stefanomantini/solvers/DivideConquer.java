package com.stefanomantini.solvers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.stefanomantini.solvers.alterations.TwoOpt;
import com.stefanomantini.solvers.objects.Tour;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.solvers.util.EuclidianDistance;
import com.stefanomantini.web.util.SolverRequestObject;

/**
 * Splits the tour into chunks and tries to brute force, followed by 2-opt
 * @author Stefano Mantini
 */
public class DivideConquer {


	public static Tour calc(ArrayList<Vehicle> vehicles, SolverRequestObject request) throws Exception {
		Tour shortestTour = null;

		int n = vehicles.size();
		if(n>0 && n<=5){
			return BruteForce.calculateBruteForce(vehicles, request);
		}else if(n >=6){
			//shuffle the list
			Collections.shuffle(vehicles);
			
			//remove startnode and add back in the end
			Vehicle start = vehicles.get(0);
			vehicles.remove(0);
			
			ArrayList<Vehicle> tour = new ArrayList<Vehicle>();
			List<List<Vehicle>> chunks = split(vehicles, 5);

			for(List<Vehicle> l : chunks){
				Tour t = BruteForce.calculateBruteForce(l, request);
				tour.addAll(t.getVehicles());
			}
			
			tour.add(0, start);
			shortestTour = new Tour(tour, EuclidianDistance.calcCollectionFromRequest(tour, request));
			shortestTour = TwoOpt.execute(shortestTour, request);
			
			
		}else{
			throw new IndexOutOfBoundsException("Divide & Conquer cannot handle large tours");
		}
		return shortestTour;
	}
	
	public static <T extends Object> List<List<T>> split(List<T> list, int targetSize) {
	    List<List<T>> lists = new ArrayList<List<T>>();
	    for (int i = 0; i < list.size(); i += targetSize) {
	        lists.add(list.subList(i, Math.min(i + targetSize, list.size())));
	    }
	    return lists;
	}

	
	
	public static <T>List<List<T>> chopIntoParts( final List<T> ls, final int iParts )
	{
	    final List<List<T>> lsParts = new ArrayList<List<T>>();
	    final int iChunkSize = ls.size() / iParts;
	    int iLeftOver = ls.size() % iParts;
	    int iTake = iChunkSize;

	    for( int i = 0, iT = ls.size(); i < iT; i += iTake )
	    {
	        if( iLeftOver > 0 )
	        {
	            iLeftOver--;

	            iTake = iChunkSize + 1;
	        }
	        else
	        {
	            iTake = iChunkSize;
	        }

	        lsParts.add( new ArrayList<T>( ls.subList( i, Math.min( iT, i + iTake ) ) ) );
	    }

	    return lsParts;
	}


}
