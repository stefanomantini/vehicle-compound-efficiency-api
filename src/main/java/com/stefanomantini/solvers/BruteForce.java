package com.stefanomantini.solvers;

import java.util.ArrayList;
import java.util.List;

import com.stefanomantini.solvers.objects.Tour;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.solvers.util.Duplicates;
import com.stefanomantini.solvers.util.EuclidianDistance;
import com.stefanomantini.web.util.SolverRequestObject;

/**
 * Brute force solver:
 * Generate all possible permutations of the original tour 
 * and choose the shortest tour
 * @author Stefano Mantini
 */
public class BruteForce {

	/**
	 * Brute Force main method
	 * generates all possible permutations of the list
	 * iterates through N! list of permutations and gets 
	 * the shortest tour
	 * throws an IOOB Exception on defined max
	 * @param l initial tour
	 * @param request request object
	 * @return Actual Shortest Tour
	 * @throws Exception
	 */
	public static Tour calculateBruteForce(List<Vehicle> l, SolverRequestObject request) throws Exception {
		double shortestTourValue = Double.MAX_VALUE;
		Tour shortestTour = null;
		int max = 9;
		
		if(l.size() >= max){
			throw new IndexOutOfBoundsException("Brute force cannot handle datasets larger than "+max);
		}
		
		//calc = ED calc code used in algorithm
		ArrayList<Tour> calcTours = new ArrayList<>();
		
		List<ArrayList<Vehicle>> tours = combinations(l, request);
		
		//calculates euclidian distance for all tours using provided request object
		for(List<Vehicle> perm : tours){
			Tour currentTour = new Tour(perm, EuclidianDistance.calcCollectionFromRequest((ArrayList<Vehicle>) perm, request) );
			if(currentTour.getTotalEuclidianDistance() < shortestTourValue){
				shortestTour = currentTour;
				shortestTourValue = currentTour.getTotalEuclidianDistance();
			}
		}
		
		return shortestTour;
	}
	
	
	
	/**
	 * Creates the outer list to contain permutations
	 * stops creation on every recursion
	 * @param l tour
	 * @return List of Lists containing All 
	 * Possible Combinations of vehicles
	 */
	public static List<ArrayList<Vehicle>> combinations(List<Vehicle> l, SolverRequestObject request) {
		List<List<Vehicle>> permutations = step(l, l.size(), new ArrayList<>());
		List<ArrayList<Vehicle>> validPerms = new ArrayList<ArrayList<Vehicle>>();
		
		if(request.getSolver().equals("divide-and-conquer")){
			for(List<Vehicle> perm : permutations){
				//if is a valid permutation for constraints
				if(!Duplicates.hasDuplicates(perm)){
					validPerms.add((ArrayList<Vehicle>) perm);
				}
			}
		}else{
			for(List<Vehicle> perm : permutations){
				//if is a valid permutation for constraints
				if(!Duplicates.hasDuplicates(perm) && perm.get(0).getInvariant().equals("STARTNODE")){
					validPerms.add((ArrayList<Vehicle>) perm);
				}
			}
		}
		
		return validPerms;
	}

	
	
	/**
	 * Recursive method to find every possible permutation of vehicles and
	 * stores the tours as an arraylist of arrays- n! permutations
	 * this could potentially be replaced by java8 streams
	 * @param l
	 * @param k
	 * @param result
	 * @return
	 */
	public static List<List<Vehicle>> step(List<Vehicle> l, int k, List<List<Vehicle>> result) {
		
		// complete
		if (k == 0) {
			return result;
		}

		// Start with [[1], [2], [3]] in result
		if (result.size() == 0) {
			for (Vehicle i : l) {
				ArrayList<Vehicle> subList = new ArrayList<>();
				subList.add(i);
				result.add(subList);
			}

			// recurse
			return step(l, k - 1, result);
		}		

		// Cross result with input. 2 entries per sub list. Then 3 then...
		List<List<Vehicle>> newResult = new ArrayList<List<Vehicle>>();
		for (List<Vehicle> subList : result) {
			for (Vehicle i : l) {
				List<Vehicle> newSubList = new ArrayList<Vehicle>();
				newSubList.addAll(subList);
				newSubList.add(i);
				newResult.add(newSubList);
			}
		}

		// recurse
		return step(l, k - 1, newResult);
	}
	

	
	

}
