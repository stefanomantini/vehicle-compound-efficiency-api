package com.stefanomantini.solvers;

import java.util.ArrayList;
import java.util.Iterator;

import com.stefanomantini.solvers.objects.Tour;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.solvers.util.EuclidianDistance;
import com.stefanomantini.web.util.SolverRequestObject;

/**
 * Tries nearest-neighbour on all possible 
 * vehicles in the tour as startnode
 * @author Stefano Mantini
 *
 */
public class RepeatedNearestNeighbour {

	/**
	 * Repeated Nearest Neighbor Algorithm: For each of the cities, 
	 * run the nearest neighbor algorithm with that city as the
	 * starting point, and choose the resulting tour with the 
	 * shortest total distance.
	 * @param unvisited
	 * @return
	 */
	public static Tour calculateRepeatedNearestNeighbour(ArrayList<Vehicle> unvisited, SolverRequestObject request) {
		ArrayList<Vehicle> visited = new ArrayList<Vehicle>();
		Tour shortestTour = null;
		double min = Double.MAX_VALUE;
		Vehicle start = null;
		
		//find start node
		for (Iterator<Vehicle> iterator = unvisited.iterator(); iterator.hasNext();) {
		    Vehicle vehicle = iterator.next();
		    	//iterator.remove();
			if(vehicle.isStartNode()){
				visited.add(vehicle);
				start = vehicle;
				iterator.remove();
				break;
			}
		}
		
		//construct partial tours for each repetition
		ArrayList<ArrayList<Vehicle>> repeats = new ArrayList<ArrayList<Vehicle>>();
		for(Vehicle v: unvisited){
			ArrayList<Vehicle> temp = new ArrayList<Vehicle>();
			temp.add(start);
			temp.add(v);
			repeats.add(temp);
		}
				
		String calc = request.getCalc();
		
			//sends all repetition stubs to get resolved and checks shortest
			for(ArrayList<Vehicle> vis : repeats){
				//create unvisited from visited and initial
				ArrayList<Vehicle> unvis = new ArrayList<Vehicle>(unvisited);
				unvis.removeAll(vis);		
				
				//find NN for this iteration
				Tour thisIteration = nearestNeighbourHelper(vis, unvis, request);
				
				//check for min
				if(min>thisIteration.getTotalEuclidianDistance()){
					min = thisIteration.getTotalEuclidianDistance();
					shortestTour = thisIteration;
				}
			}
		return shortestTour;
	}

	/**
	 * Get nearest NeighbourHeper
	 * @param visited nodes been to
	 * @param unvisited nodes left to traverse
	 * @return 
	 */
	public static Tour nearestNeighbourHelper(ArrayList<Vehicle> visited, ArrayList<Vehicle> unvisited, SolverRequestObject request){
		while(unvisited.size() >= 1){
			Vehicle C = nearestNeighbourCalc(visited.get(visited.size()-1), unvisited, request);
			unvisited.remove(C);
			visited.add(C);
		}
		Tour tour = new Tour(visited, EuclidianDistance.calcCollectionFromRequest(visited, request));
		return tour;
	}

	
	
	/**
	 * Get the nearest vehicle to startnode 
	 * @param start
	 * @param unvisited
	 * @param request
	 * @return
	 */
	public static Vehicle nearestNeighbourCalc(Vehicle start, ArrayList<Vehicle> unvisited, SolverRequestObject request){
		double max = Double.MAX_VALUE;
		Vehicle shortestNextNode = null;
		for(Vehicle uVehicle: unvisited){
			double possibleDistance = EuclidianDistance.calcPairFromRequest(start, uVehicle, request); //calculates for current and next7
			if(possibleDistance < max){
				max = possibleDistance;
				shortestNextNode = uVehicle;
			}
		}
		return shortestNextNode;
	}



}
