package com.stefanomantini.web.util;

import java.util.ArrayList;

import javax.naming.directory.InvalidAttributesException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stefanomantini.config.Constants;
import com.stefanomantini.solvers.BruteForce;
import com.stefanomantini.solvers.DivideConquer;
import com.stefanomantini.solvers.MorroccoTangiers;
import com.stefanomantini.solvers.NearestNeighbour;
import com.stefanomantini.solvers.RepeatedNearestNeighbour;
import com.stefanomantini.solvers.SampledRepeatedNearestNeighbour;
import com.stefanomantini.solvers.Unsorted;
import com.stefanomantini.solvers.alterations.SegmentReverse;
import com.stefanomantini.solvers.alterations.TwoOpt;
import com.stefanomantini.solvers.objects.Tour;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.solvers.util.TestData;

/**
 * Manages dispatching data and solvers
 * @author Stefano Mantini
 *
 */
public class Dispatcher {

	private static final Logger logger = LoggerFactory.getLogger(Dispatcher.class);

	/**
	 * Request object drops through and sets shortestTour then does any 
	 * improvement algorithms with the new set if specified in request
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static Tour solverDispatch(SolverRequestObject request) throws Exception {

		Tour shortestTour = null;

		if (request.getSampleType().equals("sample")) {
			ArrayList<Vehicle> sample = TestData.generateFromSample(request.getnVehicles());
			logger.debug("Generated Sample:" + sample);

			if (request.getSolver().equals("brute-force")) {
				logger.debug("Solving using brute-force for Sample data:" + sample);
				shortestTour = BruteForce.calculateBruteForce(sample, request);
			} 
			else if (request.getSolver().equals("nearest-neighbour")) {
				logger.debug("Solving using nearest-neighbour for Sample data:" + sample);
				shortestTour = NearestNeighbour.calculateNearestNeighbour(sample, request);
			} 
			else if (request.getSolver().equals("repeated-nearest-neighbour")) {
				logger.debug("Solving using repeated-nearest-neighbour for Sample data:" + sample);
				shortestTour = RepeatedNearestNeighbour.calculateRepeatedNearestNeighbour(sample, request);
			} 
			else if (request.getSolver().equals("sampled-repeated-nearest-neighbour")) {
				logger.debug("Solving using repeated-nearest-neighbour for Sample data:" + sample);
				shortestTour = SampledRepeatedNearestNeighbour.calculateSampledRepeatedNearestNeighbour(sample,
						request);
			} 
			else if (request.getSolver().equals("nissan-morrocco-tangiers")) {
				logger.debug("Solving using Nissan Method for Sample data:" + sample);
				shortestTour = MorroccoTangiers.getNissanTour(sample, request);
			} 
			else if (request.getSolver().equals("unsorted")) {
				logger.debug("Solving unsorted for  Sample data:" + sample);
				shortestTour = Unsorted.calc(sample, request);
			} 
			else if (request.getSolver().equals("divide-and-conquer")) {
				logger.debug("Solving using Divide and Conquer for Sample data:" + sample);
				shortestTour = DivideConquer.calc(sample, request);
			}
		} 
		else if (request.getSampleType().equals("random")) {
			ArrayList<Vehicle> random = TestData.generateVehicles(request.getnVehicles());
			logger.debug("Generated Random Data:" + random);

			if (request.getSolver().equals("brute-force")) {
				logger.debug("Solving using brute-force for random data:" + random);
				shortestTour = BruteForce.calculateBruteForce(random, request);
			} 
			else if (request.getSolver().equals("nearest-neighbour")) {
				logger.debug("Solving using nearest-neighbour for random data:" + random);
				shortestTour = NearestNeighbour.calculateNearestNeighbour(random, request);
			} 
			else if (request.getSolver().equals("repeated-nearest-neighbour")) {
				logger.debug("Solving using repeated-nearest-neighbour for random data:" + random);
				shortestTour = RepeatedNearestNeighbour.calculateRepeatedNearestNeighbour(random, request);
			} 
			else if (request.getSolver().equals("sampled-repeated-nearest-neighbour")) {
				logger.debug("Solving using sampled-repeated-nearest-neighbour for random data:" + random);
				shortestTour = SampledRepeatedNearestNeighbour.calculateSampledRepeatedNearestNeighbour(random,
						request);
			} 
			else if (request.getSolver().equals("nissan-morrocco-tangiers")) {
				logger.debug("Solving using Nissan Method for Sample data:" + random);
				shortestTour = MorroccoTangiers.getNissanTour(random, request);
			} 
			else if (request.getSolver().equals("unsorted")) {
				logger.debug("Solving unsorted for  Random data:" + random);
				shortestTour = Unsorted.calc(random, request);
			} 
			else if (request.getSolver().equals("divide-and-conquer")) {
				logger.debug("Solving using Divide & Conuer for random data:" + random);
				shortestTour = DivideConquer.calc(random, request);
			}
		}
		else if (request.getSampleType().equals("mona")) {
			ArrayList<Vehicle> mona = TestData.getFromFile(Constants.DATA_MONA, request.getnVehicles());
			logger.debug("Generated Mona lisa Data:" + mona);
			if (request.getSolver().equals("brute-force")) {
				logger.debug("Solving using brute-force for Sample data:" + mona);
				shortestTour = BruteForce.calculateBruteForce(mona, request);
			} 
			else if (request.getSolver().equals("nearest-neighbour")) {
				logger.debug("Solving using nearest-neighbour for random data:" + mona);
				shortestTour = NearestNeighbour.calculateNearestNeighbour(mona, request);
			} 
			else if (request.getSolver().equals("repeated-nearest-neighbour")) {
				logger.debug("Solving using repeated-nearest-neighbour for random data:" + mona);
				shortestTour = RepeatedNearestNeighbour.calculateRepeatedNearestNeighbour(mona, request);
			} 
			else if (request.getSolver().equals("sampled-repeated-nearest-neighbour")) {
				logger.debug("Solving using sampled-repeated-nearest-neighbour for random data:" + mona);
				shortestTour = SampledRepeatedNearestNeighbour.calculateSampledRepeatedNearestNeighbour(mona,
						request);
			} 
			else if (request.getSolver().equals("nissan-morrocco-tangiers")) {
				logger.debug("Solving using Nissan Method for Sample data:" + mona);
				shortestTour = MorroccoTangiers.getNissanTour(mona, request);
			} 
			else if (request.getSolver().equals("unsorted")) {
				logger.debug("Solving unsorted for  Sample data:" + mona);
				shortestTour = Unsorted.calc(mona, request);
			} 
			else if (request.getSolver().equals("divide-and-conquer")) {
				logger.debug("Solving using Divide & Conuer for random data:" + mona);
				shortestTour = DivideConquer.calc(mona, request);
			}
		} else if (request.getSampleType().equals("data")) {
			ArrayList<Vehicle> data = request.getVehicles();
			if (request.getSolver().equals("brute-force")) {
				logger.debug("Solving using brute-force for Sample data:" + data);
				shortestTour = BruteForce.calculateBruteForce(data, request);
			} 
			else if (request.getSolver().equals("nearest-neighbour")) {
				logger.debug("Solving using nearest-neighbour for Sample data:" + data);
				shortestTour = NearestNeighbour.calculateNearestNeighbour(data, request);
			} 
			else if (request.getSolver().equals("repeated-nearest-neighbour")) {
				logger.debug("Solving using repeated-nearest-neighbour for Sample data:" + data);
				shortestTour = RepeatedNearestNeighbour.calculateRepeatedNearestNeighbour(data, request);
			} 
			else if (request.getSolver().equals("sampled-repeated-nearest-neighbour")) {
				logger.debug("Solving using repeated-nearest-neighbour for Sample data:" + data);
				shortestTour = SampledRepeatedNearestNeighbour.calculateSampledRepeatedNearestNeighbour(data, request);
			} 
			else if (request.getSolver().equals("nissan-morrocco-tangiers")) {
				logger.debug("Solving using Nissan Method for Sample data:" + data);
				shortestTour = MorroccoTangiers.getNissanTour(data, request);
			}
			else if (request.getSolver().equals("unsorted")) {
				logger.debug("Solving unsorted for data:" + data);
				shortestTour = Unsorted.calc(data, request);
			} 
			else if (request.getSolver().equals("divide-and-conquer")) {
				logger.debug("Solving using Divide & Conuer for data:" + data);
				shortestTour = DivideConquer.calc(data, request);
			}
		} else if (request.getSampleType().equals("random-seed")) {
			ArrayList<Vehicle> seed = TestData.generateSeededRandom(request.getnVehicles(), request.getSeed());
			if (request.getSolver().equals("brute-force")) {
				logger.debug("Solving using brute-force for Sample seed:" + seed);
				shortestTour = BruteForce.calculateBruteForce(seed, request);
			} 
			else if (request.getSolver().equals("nearest-neighbour")) {
				shortestTour = NearestNeighbour.calculateNearestNeighbour(seed, request);
				logger.debug("Solving using nearest-neighbour for Sample seed:" + seed);
			} 
			else if (request.getSolver().equals("repeated-nearest-neighbour")) {
				shortestTour = RepeatedNearestNeighbour.calculateRepeatedNearestNeighbour(seed, request);
			} 
			else if (request.getSolver().equals("sampled-repeated-nearest-neighbour")) {
				logger.debug("Solving using repeated-nearest-neighbour for Sample seed:" + seed);
				shortestTour = SampledRepeatedNearestNeighbour.calculateSampledRepeatedNearestNeighbour(seed, request);
			} 
			else if (request.getSolver().equals("nissan-morrocco-tangiers")) {
				logger.debug("Solving using Nissan Method for Sample seed:" + seed);
				shortestTour = MorroccoTangiers.getNissanTour(seed, request);
			}
			else if (request.getSolver().equals("unsorted")) {
				logger.debug("Solving unsorted for  Sample data:" + seed);
				shortestTour = Unsorted.calc(seed, request);
			} 
			else if (request.getSolver().equals("divide-and-conquer")) {
				logger.debug("Solving using Divide & Conuer for Seeded data:" + seed);
				shortestTour = DivideConquer.calc(seed, request);
			}
		}

		// alter Tours after using inital Algo
		if (request.getAlterTour().equals("pairwise-exchange")) {
			shortestTour = TwoOpt.execute(shortestTour, request);
		} 
		else if (request.getAlterTour().equals("segment-reverse")) {
			shortestTour = SegmentReverse.alterTour(shortestTour, request);
		}

		// debug statement for wrong inputs
		if (shortestTour == null) {
			logger.debug("Wrong sample type mapping on request, no tour generated");
	        throw new InvalidAttributesException();
		}
		return shortestTour;

	}
}
