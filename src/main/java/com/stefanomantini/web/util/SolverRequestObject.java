package com.stefanomantini.web.util;

import java.util.ArrayList;

import javax.validation.constraints.*;

import com.stefanomantini.solvers.objects.Vehicle;

/**
 * Request from user specifies granularly what params are available
 * The reasoning behind POSTing an object is to allow for data
 * to be passed in
 * @author Stefano Mantini
 *
 */
public class SolverRequestObject {
	
	@NotNull
	private String solver;
	
	@NotNull
	private String calc;
	
	@NotNull
	private String sampleType;
	
	private int nVehicles;
	
	private int walkWeight;
	
	private long seed;
	
	private String alterTour;
		
	private ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();


	public SolverRequestObject(){
		this.solver = "";
		this.calc = "";
		this.sampleType = "";
		this.nVehicles = 0;
		this.vehicles = null;
		this.walkWeight = 0;
		this.alterTour = "";
		this.seed = 149L;
	}
	
	public SolverRequestObject(String solver, String calc, String sampleType, int nVehicles, int walkWeight, String alterTour, long seed, ArrayList<Vehicle> vehicles){
		this.solver = solver;
		this.calc = calc;
		this.sampleType = sampleType;
		this.nVehicles = nVehicles;
		this.vehicles = vehicles;
		this.walkWeight = walkWeight;
		this.alterTour=alterTour;
		this.seed=seed;
	}
	
	@Override
	public String toString() {
		return "SolverRequestObject [solver=" + solver + ", calc=" + calc + ", sampleType=" + sampleType
				+ ", nVehicles=" + nVehicles + ", walkWeight=" + walkWeight + ", seed=" + seed + ", alterTour="
				+ alterTour + ", vehicles=" + vehicles + "]";
	}

	public ArrayList<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(ArrayList<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	/**
	 * @return the solver
	 */
	public String getSolver() {
		return solver;
	}


	/**
	 * @param solver the solver to set
	 */
	public void setSolver(String solver) {
		this.solver = solver;
	}


	/**
	 * @return the calc
	 */
	public String getCalc() {
		return calc;
	}


	/**
	 * @param calc the calc to set
	 */
	public void setCalc(String calc) {
		this.calc = calc;
	}


	/**
	 * @return the sampleType
	 */
	public String getSampleType() {
		return sampleType;
	}


	/**
	 * @param sampleType the sampleType to set
	 */
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}


	/**
	 * @return the nVehicles
	 */
	public int getnVehicles() {
		return nVehicles;
	}


	/**
	 * @param nVehicles the nVehicles to set
	 */
	public void setnVehicles(int nVehicles) {
		this.nVehicles = nVehicles;
	}


	/**
	 * @return the walkWeight
	 */
	public int getWalkWeight() {
		return walkWeight;
	}


	/**
	 * @param walkWeight the walkWeight to set
	 */
	public void setWalkWeight(int walkWeight) {
		this.walkWeight = walkWeight;
	}


	/**
	 * @return the alterTour
	 */
	public String getAlterTour() {
		return alterTour;
	}


	/**
	 * @param alterTour the alterTour to set
	 */
	public void setAlterTour(String alterTour) {
		this.alterTour = alterTour;
	}

	public long getSeed() {
		return seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}



	



}
