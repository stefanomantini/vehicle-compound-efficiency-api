package com.stefanomantini.web.util;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * TODO set timeout on all rest controllers using 
 * async Callable object
 * @author Stefano Mantini
 *
 */
@SuppressWarnings("unused")
public class AsyncMethodTimer {

	static Callable<Object> complicatedCalculation = new Callable<Object>() {
		public Object call() throws Exception {
			// Calculate your stuff
			Thread.sleep(1337);
			return "42";
		}
	};
/*
	public static void main(final String[] args) {
		final ExecutorService service = Executors.newSingleThreadExecutor();

		try {
			final Future<Object> f = service.submit(complicatedCalculation);
			System.out.println(f.get(1, TimeUnit.SECONDS));
		} catch (final TimeoutException e) {
			System.err.println("Calculation took to long");
		} catch (final Exception e) {
			throw new RuntimeException(e);
		} finally {
			service.shutdown();
		}
	}
*/
}
