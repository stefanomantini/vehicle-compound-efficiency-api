package com.stefanomantini.config;

/**
 * Application constants.
 * @author Stefano Mantini
 */
public final class Constants {

	//SWAGGER Constants
	public static final String SWAGGER_TITLE = "Vehicle Compound Management";
	public static final String SWAGGER_DESC = "REST API";
	public static final String SWAGGER_VERSION = "0.0.1";
	public static final String SWAGGER_TERMS_OF_SERVICE = "cmp-tsp.herokuapp.com/#/";
	public static final String SWAGGER_CONTACT = "Stefano Mantini";
	public static final String SWAGGER_LICENCE = "MIT";
	public static final String SWAGGER_LICENCE_URL =  "MIT";
	
	public static final String DATA_MONA =  "classpath:data/mona-lisa.txt";

    private Constants() {
    }
}
